$(function() {

	var defaultSettings = {
		'textColor': "#554e48",
		"essentialColor": "#2f2f2f",
		"curveImage": 'curve-gray-wide.png',
		"leftPanelBackground": "#e6e6e6",
		"rightPanelBackground": "",
		"logoWidth": 20,
		"logoHeight": 20,
	}
	const groupSettings = {
		"all": {
			"roleColor": "#f89c3e",
			"activeGroups": [ "essential", "makeitmine", "linebreak", "masita", "fi-ta" ]
		},
		"makeitmine": {
			"roleColor": "#0280bc",
			"activeGroups": ["makeitmine"]
		},
		"essential": {
			"roleColor": "#ed1c24",
			"activeGroups": ["essential"],
			"logoWidth": 20,
			"logoHeight": 17,
			},
		"linebreak": {
			"roleColor": "#3ca4f8",
			"activeGroups": ["linebreak"]
		},
		"masita": {
			"roleColor": "#c4181d",
			"activeGroups": ["masita"]
		},
		"fi-ta": {
			"roleColor": "#000",
			"activeGroups": ["fi-ta"],
			"logoWidth": 20,
			"logoHeight": 18,
		},
		"united": {
			'textColor': "#fff",
			"roleColor": "#fff",
			"groupImage": "united.png",
			"essentialColor": "#606060",
			"curveImage": 'curve-blue-wide.png',
			"leftPanelBackground": "#32a9e0",
			"rightPanelBackground": "",
		}
	}
	let baseImageUrl = 'https://essentialappliancerentals.s3-ap-southeast-2.amazonaws.com/email/signature_images/'

	const $form = $('#signature-form')
	let $renderElement = $('#signature-render')
	const $htmlElement = $('#signature-html')
	const $btnHtmlView = $('#btn-html-view')
	const iconWidth = '20px'
	var activeSettings = {};

	$("#sig-generate").click(function(e) {
		
		setRender(makeHtml())
		$btnHtmlView.removeClass('hidden')
		// Reveal the group list icons, or united logo
		if ($('#sig-group').val() !== 'united') {
			$('#united-logo').addClass('hidden')
			$('#group-list').removeClass('hidden')
		}
		else {
			$('#group-list').addClass('hidden')
			$('#united-logo').removeClass('hidden')
		}
	});

	function makeHtml() {
		const name = $('#sig-name').val();
		const role = $('#sig-role').val();
		const email = $('#sig-email').val();
		const mobile = $('#sig-mobile').val();

		// Load active group settings
		const group = $('#sig-group').val();
		activeSettings = {...defaultSettings, ...groupSettings[group]}

		const roleColor = activeSettings.roleColor
		// The style section here was copy/pasted from the web page output using the CSS Used plugin (chrome).
		var html = `
<style>
	*,::after,::before{box-sizing:border-box;}
	h1,h2{margin-top:0;margin-bottom:.5rem;}
	p{margin-top:0;margin-bottom:1rem;}
	b{font-weight:bolder;}
	img{vertical-align:middle;border-style:none;}
	table{border-collapse:collapse;}
	h1,h2{margin-bottom:.5rem;font-weight:500;line-height:1.2;}
	h1{font-size:2.5rem;}
	h2{font-size:2rem;}
	@media print{
	*,::after,::before{text-shadow:none!important;box-shadow:none!important;}
	img,tr{page-break-inside:avoid;}
	h2,p{orphans:3;widows:3;}
	h2{page-break-after:avoid;}
	}
	div#signature-render{font-family:'Segoe UI', Verdana, sans-serif;}
	td{font-size:60%;}
	h1{font-size:1.75em;}
	h2{font-size:1.5em;}
	.hidden{display:none;}
	#left-panel{width:20em;padding-left:2em;}
	#right-panel{padding-left:0px;background:#fff;background-height:120px;background-repeat:no-repeat;}
	#user-detail{padding:0.5em;padding-top:1em;}
	#user-detail p{width:30em;margin-bottom:0.25em;}
	#right-panel td{padding-top:0.5em;}
	#group-list{margin-top:2em;margin-bottom:2em;margin-left:2em;padding:0.5em;line-height:0.5em;color:#2f2f2f;}
	#united-logo{margin-top:1.5em;margin-bottom:3em;margin-left:2em;padding:0.5em;line-height:0.5em;}
	#group-list p{margin-bottom:0.1em;}
	img.logo{margin-left:1em;margin-right:1em;}
	td{font-family:'Segoe UI', Verdana, sans-serif;font-size:60%;}
	</style>
		<table style="color: ${activeSettings.textColor}; min-width:8.6em;">
			<tbody>
			<tr>
				<td id="left-panel" style="color: ${activeSettings.textColor}; background: ${activeSettings.leftPanelBackground};" >
					<table>
						<tbody>
							<tr>
								<td style="color:${activeSettings.textColor}"><h2 style="font-size: 1.7em;"><b><i>${name}</i></b></h2></td>
							</tr>
							<tr>
								<td><i><h2 style="color:${roleColor};"><i>${role}</i></h2></i></td>
							</tr>
							<tr>
								<td id="user-detail" style="color: ${activeSettings.textColor}; border-left:1px solid ${roleColor}; font-weight:bold">
									<p>${mobile}</p>
									<p>${email}</p>
									<p>Building 31, 885 Mountain Highway, Bayswater 3153</p>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				<td id="right-panel" style="color: ${activeSettings.textColor}; background-image: url('${baseImageUrl + activeSettings.curveImage}');">
					<!--[if gte mso 9]>
      					<v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;width:400pt;">
						<v:fill type="tile" src="${baseImageUrl + activeSettings.curveImage}" />
        				<v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
      				<![endif]-->
					<table cellspacing="10" border="0">
						<tbody>
						<tr>
							<td style="color: ${activeSettings.essentialColor}; padding-left:9em; padding-right:2em;">
								<h1 style="font-size:2.4em;"><b><i>Essential<br/>Group</i></b></h1>
							</td>
							<td>
								<table cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="groupLogo" style="padding-left:1.5em;padding-right:1em;">
												<img 
													src="${getImageUrl('makeitmine', group)}"
													style="${getImageStyle('makeitmine', group)}"
													width="${activeSettings.logoWidth}"
													height=${activeSettings.logoHeight}"
													alt="${group}-logo">
											</td>
											<td class="groupUrl">
												<b>${getGroupUrl('makeitmine')}</b>
											</td>
										</tr>
										<tr>
											<td class="groupLogo" style="padding-left:1.5em;padding-right:1em; border-left:3px solid ${roleColor};">
												<img 
													src="${getImageUrl('essential', group)}"
													style="${getImageStyle('essential', group)}"
													width="${activeSettings.logoWidth}"
													height=${activeSettings.logoHeight}"
													alt="${group}-logo">
											</td>
											<td class="groupUrl">
												<b>${getGroupUrl('essential')}</b>
											</td>
										</tr>
										<tr>
											<td class="groupLogo" style="padding-left:1.5em;padding-right:1em;border-left:3px solid ${roleColor};">
												<img 
													src="${getImageUrl('linebreak', group)}"
													style="${getImageStyle('linebreak', group)}"
													width="${activeSettings.logoWidth}"
													height=${activeSettings.logoHeight}"
													alt="${group}-logo">
											</td>
											<td class="groupUrl">
												<b>${getGroupUrl('linebreak')}</b>
											</td>
										</tr>
										<tr>
											<td class="groupLogo" style="padding-left:1.5em;padding-right:1em;border-left:3px solid ${roleColor};">
												<img 
													src="${getImageUrl('masita', group)}"
													style="${getImageStyle('masita', group)}"
													width="${activeSettings.logoWidth}"
													height=${activeSettings.logoHeight}"
													alt="${group}-logo">
											</td>
											<td class="groupUrl">
												<b>${getGroupUrl('masita')}</b>
											</td>
										</tr>
										<tr>
											<td class="groupLogo" style="padding-left:1.5em;padding-right:1em;">
												<img 
													src="${getImageUrl('fi-ta', group)}"
													style="${getImageStyle('fi-ta', group)}"
													width="${activeSettings.logoWidth}"
													height=${activeSettings.logoHeight}"
													alt="${group}-logo">
											</td>
											<td class="groupUrl">
												<b>${getGroupUrl('fi-ta')}</b>
											</td>
										</tr>
									</tbody>
								</table>
				</td>
			</tr>
			</tbody>
		</table>
		<!--[if gte mso 9]>
   			</v:textbox>
   				</v:rect>
   		<![endif]-->
	</td>
</tr>
			<tr>
			<td colspan="3" class="confidentiality-note" style="color: #666;width:45em;">
			<br/><br/>
			<b>Confidentiality Note:</b>
			This email and any files transmitted with it are confidential and intended solely for the use of the individual
			or entity to whom they are addressed. If you have received this email in error please notify the system manager. This message contains
			confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate,
			distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete
			this e-mail from your system. If you are not the intended recipient you are notified that disclosing, copying, distributing or taking
			any action in reliance on the contents of this information is strictly prohibited.
			</td>
		</table>
		`.trim();
		return html
	}

	function setRender(html) {
		$renderElement.html(html)
		$htmlElement.val(html)
		//$htmlElement.val(html.replace(/(\r\n|\n|\r)/gm," "))
	}

	function getImageUrl(group, active_group) {
		var imageName
		const extension = '.png'
		if  (active_group=='all') {
			return baseImageUrl + group + extension
		}
		imageName = (group == active_group) ? group : group + '-bw'
		return baseImageUrl + imageName + extension
	}

	function getImageStyle(group, active_group) {
		style = `width: ${iconWidth};`
		if (group == 'fi-ta' || active_group !== 'fi-ta') {
			return style;
		}
		return  style + 'opacity: 0.5;'
	}

	function getGroupUrl(group, active_group) {
		url = group
		if (group == 'essential') {
			url = group + '.net.au'
		}	
		else {
			url = group + '.com.au'
		}
		if (active_group == 'all' || group == active_group) {
			url = '<b>' + url + '</b>';
		}
		// Need to embed link styling here, won't work in email in style section
		return "<a href=\"https://www." + url + "\" target=\"_blank\" style=\"color: #2f2f2f;\">" + url + "</a>";
	}

	$('#btn-html-view').click(function(e) {
		$htmlElement.toggleClass('hidden');
	})
})
