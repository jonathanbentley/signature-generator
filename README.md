# README #

### What is this repository for? ###

* This was intended as a throw-away tool to make signature changes across all
  group signatures in a consistent and repeatable way.

* The tool generates a signature preview and html text that can be copy/pasted
  into the <body> tag of a new outlook email signature.  The end goal being to
  use this tool to generate static html signatures that can be attached to
  emails leaving the outgoing server.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

